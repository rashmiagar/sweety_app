FactoryGirl.define do
  factory :glucose_reading do
  	patient
	amount Faker::Commerce.price
	recorded_date {Faker::Time.between(Time.now - 1, Time.now)  }  
  end
end
