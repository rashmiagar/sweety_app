require 'rails_helper'

RSpec.describe ReadingsController, type: :controller do
  describe 'create' do
    before :each do
      account = FactoryGirl.create(:account, accountable: FactoryGirl.create(:patient))
      sign_in account
    end

    context 'for valid params' do
      before do
        post :create, reading: {date: Date.today.strftime('%d-%m-%Y') ,time: "7:00am", description: "before breakfast", amount: 1}
      end

      it 'should create a new reading' do
        expect(GlucoseReading.count).to eq(1)
      end
    end

    context 'for invalid params' do
      before do
        post :create, reading: {date: Date.today.strftime('%d-%m-%Y') ,time: "7:00am", description: "before breakfast"}
      end

      it 'should not create a GlucoseReading object' do
        expect{
          post :create, reading: {date: Date.today.strftime('%d-%m-%Y') ,time: "7:00am", description: "before breakfast"}
        }.to_not change(GlucoseReading, :count)
      end

      it "should give error on reading object" do
        expect(assigns(:reading_obj).errors.empty?).to_not eq(true)
      end

      it 'check flash message' do
        expect(flash[:error]).to include("Amount can't be blank")
      end
    end
  end

  describe 'reports_daily' do
    before :each do
      account = FactoryGirl.create(:account, accountable: FactoryGirl.create(:patient))
      sign_in account

      get :reports_daily, date: Date.today.strftime('%d-%m-%Y')
    end

    let!(:readings) { FactoryGirl.create_list(:glucose_reading, 4, recorded_date: Date.today)}

    it "should assign correct reading object" do
      expect(assigns(:readings)).to match_array readings
    end
    it "should render correct page" do
      expect(response).to render_template("reports")
    end
  end
end
