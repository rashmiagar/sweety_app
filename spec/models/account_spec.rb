require 'rails_helper'

RSpec.describe Account, type: :model do
  it { is_expected.to belong_to(:accountable) }

  context 'check callback' do
    it 'when account is created, patient is also created' do
      @account = FactoryGirl.create(:account)
      expect(Account.count).to eq(1)
      expect(Patient.count).to eq(1)
    end
  end
end
