require 'rails_helper'

RSpec.describe GlucoseReading, :type => :model do
  describe "validations" do
	  it { is_expected.to validate_presence_of(:amount)}
    it { is_expected.to validate_presence_of(:recorded_date)}
    it { is_expected.to validate_numericality_of(:amount)}
  end

  describe "associations" do
  	subject { FactoryGirl.create(:glucose_reading) }

    it { is_expected.to belong_to(:patient) }
  end

  describe "callbacks" do
    let!(:readings) {FactoryGirl.create_list(:glucose_reading, 3)}
        
    it "should let a reading be created if fewer than 4" do
      expect{FactoryGirl.create(:glucose_reading)}.to change{GlucoseReading.count}.by(1)
    end

    it "should not let a reading be created if 4 entries" do
      expect{FactoryGirl.create_list(:glucose_reading, 2)}.to raise_error(ActiveRecord::RecordNotSaved)
    end
  end
end