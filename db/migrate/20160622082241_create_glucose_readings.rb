class CreateGlucoseReadings < ActiveRecord::Migration
  def change
    create_table :glucose_readings do |t|
      t.integer :amount, null: false
      t.datetime :recorded_date, null: false
      t.string :description
      t.references :patient
      t.timestamps null: false
    end
  end
end
