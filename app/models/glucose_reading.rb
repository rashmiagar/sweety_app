class GlucoseReading < ActiveRecord::Base
  include CheckValidation

  belongs_to :patient

  validates_presence_of :amount, :recorded_date

  validates_numericality_of :amount
  before_create :check_reading_limit



  scope :daily_report, ->(date) {includes(:patient).where("str_to_date(recorded_date,'%Y-%m-%d') =?", date)}
  scope :monthdate_report, ->(date) {includes(:patient).where("str_to_date(recorded_date, '%Y-%m-%d') BETWEEN ? AND ?", Time.parse(date).beginning_of_month.strftime('%Y-%m-%d'), date )}
  scope :monthly_report, ->(date) {includes(:patient).where("str_to_date(recorded_date, '%Y-%m-%d') BETWEEN ? AND ?", Time.parse(date).prev_month.strftime('%Y-%m-%d').next, date )}

end
