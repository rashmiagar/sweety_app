module CheckValidation 
  extend ActiveSupport::Concern

  PER_DAY_ENTRY_LIMIT = 4

	def check_reading_limit
    count = GlucoseReading.where("str_to_date(recorded_date,'%Y-%m-%d')=?", self.recorded_date.strftime("%Y-%m-%d")).count
    if count < PER_DAY_ENTRY_LIMIT
      true
    else
      self.errors[:base] << "You cannot enter any more readings for this date"
      false
    end
  end
end