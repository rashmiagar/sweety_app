class Patient < ActiveRecord::Base
	has_one :account, as: :accountable
	has_many :glucose_readings
end
