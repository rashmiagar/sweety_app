// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap.min
//= require bootstrap-datepicker
//= require jquery.timepicker.min
//= require_self
//= require_tree .


$(function() {
    $("#new_form").validate({
    
        rules: {
            "reading[date]": {required: true, remote: "/check_reading_limit"},
            "reading[time]": {required: true},
            "reading[amount]": {required: true, digits: true, max: 600, min: 10}
        }
    });



	$("ul.nav-tabs a").click(function (e) {
  		// e.preventDefault();
    	$('ul.nav-tabs a.active').removeClass('active');
    	$(this).addClass('active');
	});

    $('#datepicker, #datepicker2').datepicker({
        autoclose: true,
    	format: 'dd-mm-yyyy',
    	endDate: "Date.today()"
    	});
    // });
     $('#timepicker1').timepicker();


     $('#datepicker').on('changeDate', function(ev){
     	$('#date-daily').change();
     });
     $('#datepicker2').on('changeDate', function(ev){
        $('#date-daily').change();
     });

     $('#date-daily').change(function(){
     	console.log($('#date-daily').val());
     	var url = $('.nav-tabs .active').attr('href');
     	var dateText = $('#date-daily').val();
    		$.ajax({
    			type: "GET",
    			dataType: "script",
    			url: url,
    			data: {date: dateText}
			});
    	});
     });

