class ReadingsController < ApplicationController
	def index
		render :reports
	end

	def new
		@reading = GlucoseReading.new
	end

	def create
		d = Date.strptime(reading_params["date"], '%d-%m-%Y')
		
		if reading_params[:time].present?
			t = DateTime.parse reading_params["time"]
			@dt = DateTime.new(d.year, d.month, d.day, t.hour, t.min)
		end

		@reading_obj = GlucoseReading.new(recorded_date: @dt, description: reading_params["description"],amount: reading_params["amount"])
		@reading_obj.patient_id = current_account.accountable_id
		
		if @reading_obj.save
			flash.now[:success] = "Saved"
			render "new"
		else
			flash.now[:error] = @reading_obj.errors.full_messages
			render "new"
		end
	end

	def check_reading_limit
    count = GlucoseReading.where("str_to_date(recorded_date,'%Y-%m-%d')=?", reading_params["date"]).count
  	respond_to do |format|
  	  format.json {render json: count < CheckValidation::PER_DAY_ENTRY_LIMIT}
  	end
	end

	# gives report for specified date
	def reports_daily
		if params.has_key?("date")
			date = Time.parse(params["date"]).strftime("%Y-%m-%d")
		else
			date = Date.today.strftime('%Y-%m-%d')
		end

		@readings = current_account.accountable.glucose_readings.daily_report(date)
		@max = @readings.maximum("amount")
		@avg = @readings.average("amount").to_f
		@readings = @readings.paginate(:page => params[:page], per_page: 5)
		respond_to do |format|
			format.html { render "reports"}
			format.js { render "reports"}
		end	
	end

	# for date 15/7/16, gives report from 1/7/16 --> 15/7/16 (inclusive)
	def reports_month_date
		if params.has_key?("date")
			date = Time.parse(params["date"]).strftime("%Y-%m-%d")
		else
			date = Date.today.strftime('%Y-%m-%d')
		end
		@readings = current_account.accountable.glucose_readings.monthdate_report(date)
		@max = @readings.maximum("amount")
		@avg = @readings.average("amount").to_f
		@readings = @readings.paginate(page: params[:page], per_page: 5)
		respond_to do |format|
			format.html { render "reports"}
			format.js { render "reports"}
		end	
	end

	# for date 1/7/16, gives report from 2/6/16 --> 1/7/16 (inclusive)
	def reports_monthly
		if params.has_key?("date")
			date = Time.parse(params["date"]).strftime("%Y-%m-%d")
		else
			date = Date.today.strftime('%Y-%m-%d')
		end
		@readings = current_account.accountable.glucose_readings.monthly_report(date)
		@max = @readings.maximum("amount")
		@avg = @readings.average("amount").to_f
		@readings = @readings.paginate(page: params[:page], per_page: 5)
		respond_to do |format|
			format.html { render "reports"}
			format.js { render "reports"}
		end	
	end

	private 
	def reading_params
		params.require(:reading).permit(:date, :time, :description, :amount)
	end
end
