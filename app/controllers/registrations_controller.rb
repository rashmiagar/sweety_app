class RegistrationsController < Devise::RegistrationsController
	 before_action :configure_permitted_parameters
	 
  def create
    super
    resource.accountable_type = params[:account][:accountable_type].present? ? params[:account][:accountable_type] : "Patient"
    @user = Patient.new
    @user.name = resource.email
    @user.save
    resource.accountable_id = @user.id
    resource.save
  end

protected
def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up).push(:accountable_type)
  end
end