class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_account!
  before_filter :set_current_account
  # before_action :configure_permitted_parameters, if: :devise_controller?

  protected
  
  def set_current_account
    @current_account = current_account
  end

  # def configure_permitted_parameters
  #   devise_parameter_sanitizer.permit(:account, keys: [:email, :password, :password_confirmation, :accountable_type])
  # end
end
